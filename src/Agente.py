
import random
import string

class Agente:

    def __init__(self, numObjetos):
        self.objetos = dict()

        for i in range(1,numObjetos+1):
            self.objetos.update({i:set()})

            
    def genera_nueva_palabra(self,tamanio):
        letras = string.ascii_lowercase
        return ''.join(random.choice(letras) for i in range(tamanio))

    
    def inserta(self, obj, pal):
        self.objetos.update({obj: self.objetos.get(obj).union(set([pal]))})
        

    '''
    Regresa 1 si el agente conoce la palabra asociada al agente, elimina al
    resto de las palabras.
    Regresa 0 si el agente no conoce la palabra para el objeto y la inserta a 
    su conjunto de palabras.
    '''
    def oye(self, obj, palabra):
        if palabra in self.objetos.get(obj):
            self.objetos.update({obj: set([palabra])})
            return 1
        else:
            self.inserta(obj, palabra)
            return 0

        
    '''
    Habla con otro agente y realiza el protocolo de comunicacion para 
    todos los objetos que tenga el agente
    '''     
    def habla(self, oyente):
        for objeto in self.objetos:
            l = len(self.objetos.get(objeto))
            if l >= 1:
                p = random.randint(0, l-1)
                palabra = list(self.objetos.get(objeto))[p]
                esta = oyente.oye(objeto, palabra)
                if(esta == 1):
                    self.objetos.update({objeto: set([palabra])})
            else:
                nueva = self.genera_nueva_palabra(6)
                self.inserta(objeto, nueva)
                oyente.oye(objeto, nueva)

                
    def conjunto_palabras(self):
        total_palabras = set()
        for objeto in self.objetos:
            total_palabras = total_palabras.union(self.objetos.get(objeto))
        return total_palabras
        

    def verifica_una_palabra(self):
        for objeto in self.objetos:
            if len(self.objetos.get(objeto)) != 1:
                return False
        return True

    
    def regresa_lista_objetopalabra(self):
        a = list()
        for objeto in self.objetos:
            a.append(list(self.objetos.get(objeto))[0])
        return a

    
    
